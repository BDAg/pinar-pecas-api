const { Router }= require('express');
const routes = Router();

const pecaController = require("./controllers/pecaController");
const fabricanteController = require("./controllers/fabricanteController");
const lojaController = require("./controllers/lojaController");

routes.post("/peca/post",pecaController.post);
routes.get("/peca/get/:id",pecaController.get);
routes.get("/peca/get",pecaController.getAll);
routes.delete("/peca/delete/:id",pecaController.delete);
routes.put("/peca/put/:id",pecaController.put);

routes.post("/fabricante/post",fabricanteController.post);
routes.get("/fabricante/get/:id",fabricanteController.get);
routes.get("/fabricante/get",fabricanteController.getAll);
routes.delete("/fabricante/delete/:id",fabricanteController.delete);
routes.put("/fabricante/put/:id",fabricanteController.put);

routes.post("/loja/post",lojaController.post);
routes.get("/loja/get/:id",lojaController.get);
routes.get("/loja/get",lojaController.getAll);
routes.delete("/loja/delete/:id",lojaController.delete);
routes.put("/loja/put/:id",lojaController.put);

module.exports = routes;