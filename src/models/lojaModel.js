const mongoose = require('mongoose');

const LojaSchema = new mongoose.Schema({
    cnpj:{
        type: String,
        required: true
    },
    nome: {
        type: String,
        required: true
    },
    telefone: {
        type: String,
        required: true
    },
    endereco: {
        type: String,
        required: true
    },
    
});

module.exports = mongoose.model('Loja', LojaSchema);