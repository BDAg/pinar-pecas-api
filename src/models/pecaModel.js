const mongoose = require('mongoose');
const Loja = require('./lojaModel')
const Fabricante = require('./fabricanteModel')

const PecaSchema = new mongoose.Schema({
  nome: {
    type: String,
    required: true,
  },
  loja: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Loja",
  },
  precoUnidade: {
    type: Number,
    required: true,
  },
  precoLote: {
    type: Number,
    required: true,
  },
  fabricante: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Fabricante",
  },
  dimensao: {
    type: String,
    required: true,
  },
  material: {
    type: String,
    required: true,
  },
  descricao: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model('Peca', PecaSchema);