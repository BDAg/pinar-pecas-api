const mongoose = require('mongoose');

const fabricanteSchema = new mongoose.Schema({
    nome: {
        type: String,
        required: true
    },
    homePage: {
        type: String,
        required: true
    },
    telefone: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Fabricante', fabricanteSchema);