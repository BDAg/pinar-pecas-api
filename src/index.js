const express = require('express');
const cors = require('cors');
const routes = require('./routes');
const mongoose = require('mongoose');
const app = express();
require('dotenv').config();
const uri = process.env.ATLAS_URI || 3000;
const port = 3000

mongoose.connect(
  uri, { useNewUrlParser: true, useUnifiedTopology: true }
);

app.use(express.json());
app.use(routes);
app.use(cors());


app.listen(port, () => {
  console.log("\033[1;31;43mServer booted\033[m", "port:",port)
  console.log('db status connection: '+mongoose.connection.readyState);
});