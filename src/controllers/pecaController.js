const Peca = require('../models/pecaModel');
require('dotenv').config();

module.exports = {
    async post(request, response) {
        try {
            const { nome, loja, precoUnidade, precoLote, fabricante, dimensao, material, descricao } = request.body;

            const peca = await Peca.create({
                nome,
                loja,
                precoUnidade,
                precoLote,
                fabricante,
                dimensao,
                material,
                descricao
            });

            return response.status(200).send({ peca });
        } catch (error) {
            console.error(error);

            if (error.response) {
                console.error(error.response.body)
            }
        }
    },

    async get(request, response) {
        try {
            const { id } = request.params;

            const peca = await Peca.findOne({ _id: id });

            if (!peca) {
                return response.status(400).send({ error: 'Peça não encontrada!' });
            };

            return response.status(200).send({ peca });
        } catch (error) {
            console.error(error);

            if (error.response) {
                console.error(error.response.body)
            }
        }
    },

    async getAll(request, response) {
        Peca.find().limit().exec((err,value) => {
            response.json(value);
        })
    },

    async delete(request, response) {
        try {
            const { id } = request.params;

            const peca = await Peca.findOne({ _id: id });

            if (!peca) {
                return response.status(400).send({ error: 'Peça não encontrada!' });
            };

            await Peca.deleteOne({ _id: id });

            return response.status(200).send({ peca });
        } catch (error) {
            console.error(error);

            if (error.response) {
                console.error(error.response.body)
            }
        }
    },

    async put(request, response) {
        try {
            const { id } = request.params;

            const peca = await Peca.findOne({ _id: id });

            if (!peca) {
                return response.status(400).send({ error: 'Peça não encontrada!' });
            };

            const { nome, loja, precoUnidade, precoLote, fabricante, dimensao, material, descricao } = request.body;

            await Peca.updateOne(peca, {
                nome,
                loja,
                precoUnidade,
                precoLote,
                fabricante,
                dimensao,
                material,
                descricao
            });

            return response.status(200).send({ peca });
        } catch (error) {
            console.error(error);

            if (error.response) {
                console.error(error.response.body)
            }
        }
    },
};