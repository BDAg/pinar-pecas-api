const Loja = require('../models/lojaModel')
require('dotenv').config();


module.exports = {
    async post(request, response) {
        try {
            const { cnpj, nome, telefone, endereco } = request.body;

            const loja = await Loja.create({
                cnpj,
                nome,
                telefone,
                endereco
            });

            return response.status(200).send({ loja });
        } catch (error) {
            console.error(error);

            if (error.response) {
                console.error(error.response.body)
            }
        }
    },

    async get(request, response) {
        try {
            const { id } = request.params;

            const loja = await Loja.findOne({ _id: id });

            if (!loja) {
                return response.status(400).send({ error: 'Loja não encontrada!' });
            };

            return response.status(200).send({ loja });
        } catch (error) {
            console.error(error);

            if (error.response) {
                console.error(error.response.body)
            }
        }
    },

    async getAll(request, response) {
        Loja.find().limit().exec((err,value) => {
            response.json(value);
        })
    },

    async delete(request, response) {
        try {
            const { id } = request.params;

            const loja = await Loja.findOne({ _id: id });

            if (!loja) {
                return response.status(400).send({ error: 'Loja não encontrada!' });
            };

            await loja.deleteOne({ _id: id });

            return response.status(200).send({ loja });
        } catch (error) {
            console.error(error);

            if (error.response) {
                console.error(error.response.body)
            }
        }
    },

    async put(request, response) {
        try {
            const { id } = request.params;

            const loja = await Loja.findOne({ _id: id });

            if (!loja) {
                return response.status(400).send({ error: 'Loja não encontrada!' });
            };

            const { cnpj, nome, telefone, endereco } = request.body;

            await Loja.updateOne(loja, {
                cnpj,
                nome,
                telefone,
                endereco
            });

            return response.status(200).send({ loja });
        } catch (error) {
            console.error(error);

            if (error.response) {
                console.error(error.response.body)
            }
        }
    },
}