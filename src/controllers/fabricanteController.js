const Fabricante = require('../models/fabricanteModel');
require('dotenv').config();

module.exports = {
    async post(request, response) {
        try {
            const { nome, homePage, telefone } = request.body;

            const fabricante = await Fabricante.create({
                nome,
                homePage,
                telefone
            });

            return response.status(200).send({ fabricante });
        } catch (error) {
            console.error(error);

            if (error.response) {
                console.error(error.response.body)
            }
        }
    },

    async get(request, response) {
        try {
            const { id } = request.params;

            const fabricante = await Fabricante.findOne({ _id: id });

            if (!fabricante) {
                return response.status(400).send({ error: 'Fabricante não encontrado!' });
            };

            return response.status(200).send({ fabricante });
        } catch (error) {
            console.error(error);

            if (error.response) {
                console.error(error.response.body)
            }
        }
    },

    async getAll(request, response) {
        Fabricante.find().limit().exec((err,value) => {
            response.json(value);
        })
    },

    async delete(request, response) {
        try {
            const { id } = request.params;

            const fabricante = await Fabricante.findOne({ _id: id });

            if (!fabricante) {
                return response.status(400).send({ error: 'Fabricante não encontrado!' });
            };

            await Fabricante.deleteOne({ _id: id });

            return response.status(200).send({ fabricante });
        } catch (error) {
            console.error(error);

            if (error.response) {
                console.error(error.response.body)
            }
        }
    },

    async put(request, response) {
        try {
            const { id } = request.params;

            const fabricante = await Fabricante.findOne({ _id: id });

            if (!fabricante) {
                return response.status(400).send({ error: 'Fabricante não encontrado!' });
            };

            const { nome, homePage, telefone } = request.body;

            await Fabricante.updateOne(fabricante, {
                nome,
                homePage,
                telefone
            });

            return response.status(200).send({ fabricante });
        } catch (error) {
            console.error(error);

            if (error.response) {
                console.error(error.response.body)
            }
        }
    },
};